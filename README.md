# tp2-kotlin


**Projet Kotlin - Johan Campion & François Roullaud**

_Ce qui a été fait :_
- Les cinq écrans principaux côté front (splash, connexion, affichage des scores, écran de jeu, administration)
- Navigation d'un écran à l'autre
- Logique de jeu
- Pop-up avec le score à la fin d'une partie

_Ce qui n'a pas été terminé :_

- Binding du score 
- Design & polish (couleurs des boutons, icônes, etc)


_Ce qui n'a pas été fait :_

- Affichage du splash et transition automatique
- Sauvegarde de nouveaux marsupiaux

_Remarques :_
Tous les commits ont été réalisés sur le même compte à cause d'un problème d'accès à gitlab.
Le travail a été réalisé en peer programming lors de la période de cours (un PC pour deux), continué individuellement puis mis en commun à deux.
La version finale se trouve sur la branche "debug"
